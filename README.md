# Part 2A

## What's wrong with the structure? Could any of these have a security impact?
First the code is very unorganized. There are many differnet file types in the project and every file is in the same folder.
There are also files like hello.py that does not contribute to the project and just take up space.
A confusing structure and no unit tests makes it hard for developers to notice potential security risks.

The functions send() and search() use database queries with very poor security.
An SQL injection could be done by for example searching: ';malicious code; --

Since SQL injections is a problem you might not be able to know who actually sends a message.
The only link between user and message is the username, which a SQL injection could easily modify.

## Refactoring
* Deleted hello.py
* Moved index.html to templates folder
* Made an assets folder and moved favicon.ico and favicon.png into it
* Used secrets.token_hex() to generate app.secret_key for more security
